/*******************************************************************************
#                                                                              #
# Piphole                                                                      #
#                                                                              #
# Copyright (C) 2016 Adrià Planas                                              #
# Copyright (C) 2007 Tom Stöveken (MJPG-streamer)                              #
#                                                                              #
# This program is free software; you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation; version 2 of the License.                      #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program; if not, write to the Free Software                  #
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA    #
#                                                                              #
 *******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <signal.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <getopt.h>
#include <pthread.h>
#include <dlfcn.h>
#include <fcntl.h>
#include <syslog.h>
#include <linux/types.h>          /* for videodev2.h */
#include <linux/videodev2.h>

#include "utils.h"
#include "piphole.h"

#include "raspicam.h"
#include "httpd.h"
#include "viewer.h"

/* globals */
static globals global;

/******************************************************************************
Description.: pressing CTRL+C sends signals to this process instead of just
              killing it plugins can tidily shutdown and free allocated
              resources. The function prototype is defined by the system,
              because it is a callback function.
Input Value.: sig tells us which signal was received
Return Value: -
******************************************************************************/
static void signal_handler(int sig)
{
    int i;

    /* signal "stop" to threads */
    LOG("setting signal to stop\n");
    global.stop = 1;
    usleep(1000 * 1000);

    /* clean up threads */
    LOG("force cancellation of threads and cleanup resources\n");
    raspicam_stop();
    viewer_stop();
    httpd_stop();

    pthread_cond_destroy(&global.in.db_update);
    pthread_mutex_destroy(&global.in.db);

    usleep(1000 * 1000);

    LOG("done\n");

    closelog();
    exit(0);
    return;
}

static void help(char *progname)
{
    fprintf(stderr, "-----------------------------------------------------------------------\n");
    fprintf(stderr, "Usage: %s\n" \
            " [-h | --help ]........: display this help\n" \
            " [-v | --version ].....: display version information\n" \
            " [-b | --background]...: fork to the background, daemon mode\n", progname);
    fprintf(stderr, "-----------------------------------------------------------------------\n");
}

int main(int argc, char *argv[])
{
    int daemon = 0, i, j;

    /* parameter parsing */
    while(1) {
        int c = 0;
        static struct option long_options[] = {
            {"help", no_argument, NULL, 'h'},
            {"version", no_argument, NULL, 'v'},
            {"background", no_argument, NULL, 'b'},
            {NULL, 0, NULL, 0}
        };

        c = getopt_long(argc, argv, "h:vb", long_options, NULL);

        /* no more options to parse */
        if(c == -1) break;

        switch(c) {
        case 'v':
            printf("Piphole Version: %s\n" \
            "Compilation Date.....: %s\n" \
            "Compilation Time.....: %s\n",
            SOURCE_VERSION,
            __DATE__, __TIME__);
            return 0;
            break;

        case 'b':
            daemon = 1;
            break;

        case 'h': /* fall through */
        default:
            help(argv[0]);
            exit(EXIT_FAILURE);
        }
    }

    openlog("Piphole ", LOG_PID | LOG_CONS, LOG_USER);
    syslog(LOG_INFO, "starting application");

    /* fork to the background */
    if(daemon) {
        LOG("enabling daemon mode");
        daemon_mode();
    }

    /* ignore SIGPIPE (send by OS if transmitting to closed TCP sockets) */
    signal(SIGPIPE, SIG_IGN);

    /* register signal handler for <CTRL>+C in order to clean up */
    if(signal(SIGINT, signal_handler) == SIG_ERR) {
        LOG("could not register signal handler\n");
        closelog();
        exit(EXIT_FAILURE);
    }

    /*
     * messages like the following will only be visible on your terminal
     * if not running in daemon mode
     */
    LOG("Piphole Version.: %s\n", SOURCE_VERSION);

    // INPUT

    /* this mutex and the conditional variable are used to synchronize access to the global picture buffer */
    if(pthread_mutex_init(&global.in.db, NULL) != 0) {
        LOG("could not initialize mutex variable\n");
        closelog();
        exit(EXIT_FAILURE);
    }
    if(pthread_cond_init(&global.in.db_update, NULL) != 0) {
        LOG("could not initialize condition variable\n");
        closelog();
        exit(EXIT_FAILURE);
    }

    global.in.context = NULL;
    global.in.buf     = NULL;
    global.in.size    = 0;

    if(raspicam_init(&global)) {
        LOG("raspicam_init() return value signals to exit\n");
        closelog();
        exit(0);
    }

    if(viewer_init(&global)) {
        LOG("viewer_init() return value signals to exit\n");
        closelog();
        exit(EXIT_FAILURE);
    }

    if(httpd_init(&global)) {
        LOG("output_init() return value signals to exit\n");
        closelog();
        exit(EXIT_FAILURE);
    }

    /* start to read the input, push pictures into global buffer */
    syslog(LOG_INFO, "starting raspicam");
    if(raspicam_run()) {
        LOG("unable to run raspicam\n");
        closelog();
        return 1;
    }

    syslog(LOG_INFO, "starting viewer");
    if(viewer_run()) {
        LOG("unable to run viewer\n");
        closelog();
        return 1;
    }

    syslog(LOG_INFO, "starting httpd server");
    if(httpd_run()) {
        LOG("unable to run httpd server\n");
        closelog();
        return 1;
    }

    /* wait for signals */
    pause();

    return 0;
}
